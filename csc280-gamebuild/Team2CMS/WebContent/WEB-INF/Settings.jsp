<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="settings" tagdir="/WEB-INF/tags/settings" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
  </head>
  <body id="Admin" class="${settings.getColorTheme()}">
  
  <div id="Page">
    <template:Header />
    <template:Nav />  
    <article>
      <h2>Website Settings</h2>
      <form method="post" action="SettingsSaveServlet.do" id="WebsiteSettings">
      
        <label for="Title">Website Title:</label> <input type="text" name="Title" id="Title" value="${settings.getTitle()}" />
        <br /><br />
        
        <settings:ColorThemeSelect />
        <br /><br />
        
        <settings:BlogEnableSelect />
		<br /><br />
		
		<settings:CommentsEnableSelect />
		<br /><br />
		
        <settings:GalleryEnableSelect />
		<br /><br />
				
		<settings:AboutEnableSelect />
        <br /><br />
        
        <!-- SUBMIT -->
        <input type="submit" value="Save" />
       
       <h3>Links</h3>
       
       <ul>
       	 <li><a href="CreatePost.jsp">New Post</a></li>
       	 <li><a href="PostManager.do">Manage Posts</a></li>
       	 <li><a href="AboutEdit">Edit About Page</a></li>
       </ul>
      	
      </form>
    </article>
  </div>
     
  </body>
</html> 