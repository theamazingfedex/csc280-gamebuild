<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
    	<script type="text/javascript">
		function checkEnableSubmit() {
		  if (document.getElementById("fname").value == "") // some logic to determine if it is ok to go
		    {document.getElementById("upload").disabled = true;
		    document.getElementById("upload").src = "upload_disabled.png";}
		  else // in case it was enabled and the user changed their mind
		    {document.getElementById("upload").disabled = false;
		    document.getElementById("upload").src = "upload.png";}
		}
</script>
  </head>
  <body id="Gallery" class="Default">
  <div id="Page">
    <template:Header />
    <template:Nav />
    <h2>Upload Image</h2>
    <!-- UPLOAD -->
    <p>File types allowed: JPEG (or JPG), GIF, and PNG images.</p>
    <form method="post" action="UploadToDatabase.do" enctype="multipart/form-data">
    	<label for="fname">Uploaded file:</label> <input type="file" name="fname" id="fname" onchange="javascript:checkEnableSubmit()" /><br />
    	<label for="description">Description:</label> <input type="text" name="description" id="description" /><br />
    	<input type="image" value="upload" id="upload" src="upload_disabled.png" alt="upload" disabled />
    </form>

    </div>
  </body>
</html> 

