<%@ tag language="java" pageEncoding="ISO-8859-1"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section class="g4 clearfix">
     <c:forEach var="image" items="${files}">
     	<div class="img">
     		<img src="GetImageServlet/${image.getId()}" />     		
     		<div class="description">
     			${image.getDescription()}
     		</div>
     		<a href="DeleteImage.do/${image.getId()}">delete</a>
     	</div>
     </c:forEach>
</section>