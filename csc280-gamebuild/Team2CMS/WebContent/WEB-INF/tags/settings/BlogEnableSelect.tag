<%@ tag language="java" pageEncoding="ISO-8859-1" import="entities.Settings" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<label for="BlogEnabled">Blog Page Enabled:</label>

<select name="BlogEnabled" id="BlogEnabled">
	<c:if test="${settings.getBlogEnabled() == 'Disabled'}">
		<option selected='true'>Disabled</option>
		<option>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getBlogEnabled() == 'Everyone'}">
		<option>Disabled</option>
		<option selected='true'>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getBlogEnabled() == 'Users only'}">
		<option>Disabled</option>
		<option>Everyone</option>
		<option selected='true'>Users only</option>
	</c:if>
</select>