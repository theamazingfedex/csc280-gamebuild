<%@ tag language="java" pageEncoding="ISO-8859-1" import="entities.Settings" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<label for="ColorTheme">Color Theme:</label>

<select name="ColorTheme" id="ColorTheme">
	<c:if test="${settings.getColorTheme() == 'Default'}">
		<option selected='true'>Default</option>		
		<option>Red</option>
		<option>Orange</option>
		<option>Yellow</option>
		<option>Green</option>
		<option>Blue</option>	
		<option>Indigo</option>
		<option>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>

	<c:if test="${settings.getColorTheme() == 'Red'}">
		<option>Default</option>		
		<option selected='true'>Red</option>
		<option>Orange</option>
		<option>Yellow</option>
		<option>Green</option>
		<option>Blue</option>
		<option>Indigo</option>
		<option>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>
	
	<c:if test="${settings.getColorTheme() == 'Orange'}">
		<option>Default</option>		
		<option>Red</option>
		<option selected='true'>Orange</option>
		<option>Yellow</option>
		<option>Green</option>
		<option>Blue</option>
		<option>Indigo</option>
		<option>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>
	
	<c:if test="${settings.getColorTheme() == 'Yellow'}">
		<option>Default</option>		
		<option>Red</option>
		<option>Orange</option>
		<option selected='true'>Yellow</option>
		<option>Green</option>
		<option>Blue</option>
		<option>Indigo</option>
		<option>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>
	
	<c:if test="${settings.getColorTheme() == 'Green'}">
		<option>Default</option>		
		<option>Red</option>
		<option>Orange</option>
		<option>Yellow</option>
		<option selected='true'>Green</option>
		<option>Blue</option>
		<option>Indigo</option>
		<option>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>

	<c:if test="${settings.getColorTheme() == 'Blue'}">
		<option>Default</option>		
		<option>Red</option>
		<option>Orange</option>
		<option>Yellow</option>
		<option>Green</option>
		<option selected='true'>Blue</option>
		<option>Indigo</option>	
		<option>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>
	
	<c:if test="${settings.getColorTheme() == 'Indigo'}">
		<option>Default</option>		
		<option>Red</option>
		<option>Orange</option>
		<option>Yellow</option>
		<option>Green</option>
		<option>Blue</option>
		<option selected='true'>Indigo</option>	
		<option>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>
	
	<c:if test="${settings.getColorTheme() == 'Violet'}">
		<option>Default</option>		
		<option>Red</option>
		<option>Orange</option>
		<option>Yellow</option>
		<option>Green</option>
		<option>Blue</option>
		<option>Indigo</option>	
		<option selected='true'>Violet</option>
		<option>Space</option>
		<option>Flower</option>
	</c:if>
	
	<c:if test="${settings.getColorTheme() == 'Flower'}">
		<option>Default</option>		
		<option>Red</option>
		<option>Orange</option>
		<option>Yellow</option>
		<option>Green</option>
		<option>Blue</option>
		<option>Indigo</option>	
		<option>Violet</option>
		<option>Space</option>
		<option selected='true'>Flower</option>
	</c:if>
	
</select>
