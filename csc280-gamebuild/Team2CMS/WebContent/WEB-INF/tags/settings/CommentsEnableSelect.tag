<%@ tag language="java" pageEncoding="ISO-8859-1" import="entities.Settings" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<label for="CommentsEnabled">Commenting Enabled:</label>

<select name="CommentsEnabled" id="CommentsEnabled">
	<c:if test="${settings.getCommentsEnabled() == 'Disabled'}">
		<option selected='true'>Disabled</option>
		<option>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getCommentsEnabled() == 'Everyone'}">
		<option>Disabled</option>
		<option selected='true'>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getCommentsEnabled() == 'Users only'}">
		<option>Disabled</option>
		<option>Everyone</option>
		<option selected='true'>Users only</option>
	</c:if>
</select>