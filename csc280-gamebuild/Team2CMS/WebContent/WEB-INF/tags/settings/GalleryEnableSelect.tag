<%@ tag language="java" pageEncoding="ISO-8859-1" import="entities.Settings" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<label for="GalleryEnabled">Gallery Page Enabled:</label>

<select name="GalleryEnabled" id="GalleryEnabled">
	<c:if test="${settings.getGalleryEnabled() == 'Disabled'}">
		<option selected='true'>Disabled</option>
		<option>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getGalleryEnabled() == 'Everyone'}">
		<option>Disabled</option>
		<option selected='true'>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getGalleryEnabled() == 'Users only'}">
		<option>Disabled</option>
		<option>Everyone</option>
		<option selected='true'>Users only</option>
	</c:if>
</select>