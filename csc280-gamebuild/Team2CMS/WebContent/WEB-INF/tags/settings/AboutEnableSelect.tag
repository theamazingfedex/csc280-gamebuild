<%@ tag language="java" pageEncoding="ISO-8859-1" import="entities.Settings" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<label for="AboutEnabled">About Page Enabled:</label>

<select name="AboutEnabled" id="AboutEnabled">
	<c:if test="${settings.getAboutEnabled() == 'Disabled'}">
		<option selected='true'>Disabled</option>
		<option>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getAboutEnabled() == 'Everyone'}">
		<option>Disabled</option>
		<option selected='true'>Everyone</option>
		<option>Users only</option>
	</c:if>

	<c:if test="${settings.getAboutEnabled() == 'Users only'}">
		<option>Disabled</option>
		<option>Everyone</option>
		<option selected='true'>Users only</option>
	</c:if>
</select>