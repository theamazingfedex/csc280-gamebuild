<%@ tag language="java" pageEncoding="ISO-8859-1" import="java.util.*,entities.Post" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
     <c:forEach var="post" items="${posts}">
     	<div class="post">
     	<div class="title">
     		${post.getTitle()}
     	</div>
     		<div class="content">
     			${post.getPostContent().split("<!-- more -->")[0]}
     			<a href="PostViewServlet?viewpost=${post.getPostId()}">view</a>
     		</div>
     	</div>
     </c:forEach>
</section>