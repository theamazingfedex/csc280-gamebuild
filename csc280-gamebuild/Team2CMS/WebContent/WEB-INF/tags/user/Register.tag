<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div>
	<form method="POST" action="Register">
		<table>
		<tr><td><c:forEach var="error" items="${errors }">
      	${error }
      </c:forEach></td></tr>
			<tr>
				<td>E-mail:</td>
				<td><input type="text" name="email" /></td>
			</tr>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" /></td>
			</tr>
			<tr>
				<td colspan=2>
					<input type="submit" value="Register" />
					<input type="reset" value="Clear" />
				</td>
			</tr>
		</table>
	</form>
</div>