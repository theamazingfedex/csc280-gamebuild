<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="LinksForLoggingOn">
	<c:choose>
	<c:when test="${curUser == null}">
		<a href="Login">Log on</a> /
		<a href="Register.jsp">Register</a>
		<c:if test='${curUser.role == "admin" }'>
		 /
		</c:if>
	</c:when>
	<c:when test="${curUser != null}">
		<a href="LogOut">Log off</a> /
		<a href="Update.jsp">
	     	${curUser.name }
		</a>
		<c:if test='${curUser.role == "admin" }'>
		 /
		</c:if>
	</c:when>
	</c:choose>
</div>