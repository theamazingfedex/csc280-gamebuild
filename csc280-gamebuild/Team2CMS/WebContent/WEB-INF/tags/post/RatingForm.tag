<%@ tag language="java" pageEncoding="ISO-8859-1" %>

<form method="post" action="PostRateServlet.do" onsubmit="return validateRatingForm()">
	<input type="hidden" name="postId" value="${post.getPostId()}" />
	<select name="rating" id="rating">
		<option></option>
		<option>1</option>
		<option>2</option>
		<option>3</option>
		<option>4</option>
	</select>
	<input type="submit" value="Rate" />
</form>
