<%@ tag language="java" pageEncoding="ISO-8859-1" import="entities.Settings" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav>
  <ul>
  
  	<c:if test="${settings.getBlogEnabled() != 'Disabled'}">
  		<li><a href="Blog">Blog</a></li>
  	</c:if>
    
    <c:if test="${settings.getGalleryEnabled() != 'Disabled'}">
    	<li><a href="Gallery">Gallery</a></li>
    </c:if>
    
    <c:if test="${settings.getAboutEnabled() != 'Disabled'}">
    	<li><a href="About">About</a></li>
    </c:if>
    
  </ul>
</nav>