<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
  </head>
  <body id="About" class="${settings.getColorTheme()}">
  
  <div id="Page">
    <template:Header />
    <template:Nav />  
    <article id="AboutContent">
      ${aboutContent}
    </article>
  </div>
       
  </body>
</html> 