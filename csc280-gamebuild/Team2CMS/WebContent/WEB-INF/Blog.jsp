<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/user" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
  </head>
  <body id="Index" class="${settings.getColorTheme()}">
  
  <div id="Page">
    <template:Header />
    <template:Nav />
    <article>
   	  <c:if test='${curUser.role == "admin" }'>
      	<user:LinksForAdmin />
      </c:if>
      <user:LinksForLoggingOn />  
      <template:Posts />  
    </article>
  </div>
       
  </body>
