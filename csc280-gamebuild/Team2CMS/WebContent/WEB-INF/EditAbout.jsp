<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
  </head>
  <body id="About" class="Default">
  
  <div id="Page">
    <template:Header />
    <template:Nav />  
    <article>
    
      <form method="post" action="AboutEditSave.do">
          <textarea rows="40" cols="95" name="Content" id="Content">${aboutContent}</textarea>
          <input type="submit" value="Update" />
      </form>
        
    </article>
  </div>
       
  </body>
</html> 