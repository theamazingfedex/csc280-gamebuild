<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="post" tagdir="/WEB-INF/tags/post" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<script>
    	function validateRatingForm() {
			var ratingSelection = document.getElementById("rating");
			if(ratingSelection.selectedIndex == 0) { alert("Please select a rating first."); return false; }
		}
    	function validateCommentForm() {
			var commentTextArea = document.getElementById("comment");
			if(commentTextArea.value == "") { alert("Cannot submit an empty comment."); return false; }
		}
    	</script>
    	<title>Group Lab</title>
  </head>
  <body id="Index" class="${settings.getColorTheme()}">
  
  <div id="Page">
    <template:Header />
    <template:Nav />  
    <article>
		
		<post:Post />
		
		<post:Rating />
		<post:RatingForm />
				
		<post:Comments />
		<post:CommentForm />
				
    </article>
  </div>

  </body>
</html> 