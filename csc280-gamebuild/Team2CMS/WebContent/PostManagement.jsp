<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ page import="java.util.List" %>
<%@ page import="entities.Post" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   	<link rel="stylesheet" type="text/css" href="style.css" />
   	<title>Group Lab</title>
 </head>
<body id="Index" class="Default">
  <div id="Page">
    <template:Header />
    <template:Nav />   
    <article>
    <h2>Admin Post Management</h2>
	<p>Number of posts: ${posts.size()}</p>
	<table border="1">
	<tr>
		<th>Id</th>
		<th>Title</th>
		<th>Author</th>
		<th>Post Date</th>
		<th colspan="2"></th>
	</tr>
		<!--  START LOOP FOR EACH ROW IN THE DATABASE TABLE -->	
		<c:forEach var="post" items="${posts}">
		<tr>
			<td>${post.getPostId()}</td>
			<td>${post.getTitle()}</td>
			<td>${post.getAuthor()}</td>
			<td>${post.getPostDate()}</td>
			
			<td>
				<form method="get" action="EditPostServlet.do">
					<input type="hidden" name="crudType" value="update"/>
					<input type="hidden" name="postid" value="${post.getPostId()}"/>
					<input type="hidden" name="posttitle" value="${post.getTitle()}"/>
					<input type="hidden" name="postcontent" value="${post.getPostContent()}" />
					<input type="submit" value="edit" />
				</form>
			</td>
			<td>
				<form method="post" action="PostManager.do">
					<input type="hidden" name="crudType" value="delete">
					<input type="hidden" name="postid" value="${post.getPostId()}">
					<input type="submit" value="delete"/>
				</form>
			</td>
		</tr>
		</c:forEach>
		<!-- END LOOP -->
	</table>
	<p><a href="CreatePost.jsp">Create Post</a>
	</article>
</body>
</html>