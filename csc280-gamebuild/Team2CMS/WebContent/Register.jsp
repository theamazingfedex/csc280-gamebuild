<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/user" %>

<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
  </head>
  <body id="Register" class="Default">
  
  <div id="Page">
    <template:Header />
    <template:Nav />
    <article>
      <h2>Register</h2>
      <user:Register />
    </article>
  </div>
       
  </body>
</html>