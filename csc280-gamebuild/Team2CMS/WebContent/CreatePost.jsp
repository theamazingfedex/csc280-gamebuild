<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
  </head>
  <body id="Index" class="Default">
  
  <div id="Page">
    <template:Header />
    <template:Nav />
    <article>
      <h2>Create Post</h2>
        <form method="post" action="PostManager.do">
          <input type="hidden" name="crudType" value="create">
          <label for="Title">Title:</label> <input type="text" name="Title" id="Title" />
          <textarea rows="40" cols="95" name="Content" id="Content"></textarea>
          <input type="submit" value="Post" />
        </form>
    </article>
  </div>
       
  </body>
</html>