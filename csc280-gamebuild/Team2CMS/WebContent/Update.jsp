<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/user" %>

<!DOCTYPE html>

<html>
  <head>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab</title>
  </head>
  <body id="Index" class="Default">
  
  <div id="Page">
    <template:Header />
    <template:Nav />
    <article>
      <h2>Update Profile</h2>
      <user:EditProfile />
    </article>
  </div>
       
  </body>
</html>