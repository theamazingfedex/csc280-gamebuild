package entities;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
public class Post {
	
	@Id
	@GeneratedValue
	private int postId;
	
	@Column(nullable = false)
	private String title;
	
	@Lob
	@Column(nullable = false)
	private String postContent;
	
	@Column(nullable = false)
	private String author;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date postDate;
	
	@Column(nullable=false)
	private double rating;
	
	@OneToMany(targetEntity=entities.Comment.class, cascade=CascadeType.REMOVE, mappedBy="parentalPost")
	private List<Comment> comments;

	@ManyToOne
	private User poster;
		
	public User getPoster(){
		return this.poster;
	}
	public void setPoster(User poster){
		this.poster = poster;
	}
	
	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
	
	public String getAuthor(){
		return this.author;
	}
	public void setAuthor(String author){
		this.author = author;
	}
	
	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public String getPostContent() {
		return postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	
	public void submitNewRating(int rating) {
		if(this.rating==0.0) { this.rating = rating; }
		else { this.rating = (this.rating + rating) / 2; }
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	public void setComments(List<Comment> comments){
		this.comments = comments;
	}
	
	public void addComment(Comment comment){
		this.comments.add(comment);
	}
	public void removeComment(Comment comment){
		this.comments.remove(comment);
	}
}