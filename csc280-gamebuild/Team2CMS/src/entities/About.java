package entities;

import javax.persistence.*;

@Entity
public class About {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(nullable=true)
	private String content;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
