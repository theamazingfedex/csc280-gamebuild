package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Settings {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column(nullable = false)
	private String title;
	
	@Column(nullable = false)
	private String colorTheme;
	
	@Column(nullable = false)
	private String blogEnabled;
	
	@Column(nullable = false)
	private String galleryEnabled;
	
	@Column(nullable = false)
	private String aboutEnabled;
	
	@Column(nullable = false)
	private String commentsEnabled;
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}

	public String getColorTheme() {
		return colorTheme;
	}

	public void setColorTheme(String colorTheme) {
		this.colorTheme = colorTheme;
	}

	public String getBlogEnabled() {
		return blogEnabled;
	}

	public void setBlogEnabled(String blogEnabled) {
		this.blogEnabled = blogEnabled;
	}

	public String getGalleryEnabled() {
		return galleryEnabled;
	}

	public void setGalleryEnabled(String galleryEnabled) {
		this.galleryEnabled = galleryEnabled;
	}

	public String getAboutEnabled() {
		return aboutEnabled;
	}

	public void setAboutEnabled(String aboutEnabled) {
		this.aboutEnabled = aboutEnabled;
	}

	public String getCommentsEnabled() {
		return commentsEnabled;
	}

	public void setCommentsEnabled(String commentsEnabled) {
		this.commentsEnabled = commentsEnabled;
	}
}
