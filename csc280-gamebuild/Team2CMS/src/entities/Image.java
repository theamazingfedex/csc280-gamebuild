package entities;

import javax.persistence.*;

@Entity
public class Image {

	@Id @GeneratedValue
	private int id;
	
	@Column(nullable = true)
	private String description;
	
	@Column(nullable = false)
	private String contentType;
	
	@Column(nullable = false)
	private byte[] content;
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}
}
