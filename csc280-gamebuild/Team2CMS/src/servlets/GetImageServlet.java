package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.ImageEJB;
import entities.Image;

@WebServlet("/GetImageServlet/*")
public class GetImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB ImageEJB imageEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getPathInfo().split("/")[1]);
		Image img = imageEJB.findImage(id);
		response.setContentType(img.getContentType());
		response.getOutputStream().write(img.getContent());
	}

}
