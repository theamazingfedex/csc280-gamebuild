package servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.UserEJB;
import entities.User;

/**
 * Servlet implementation class EditProfileServlet
 */
@WebServlet("/EditProfile")
@DeclareRoles({"admin", "user"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
rolesAllowed = {"admin", "user"}),
@HttpMethodConstraint(value = "POST",
rolesAllowed = {"admin", "user"})})
public class EditProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    @EJB
    UserEJB userManager;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String c_password = request.getParameter("c_password");
		if (name.isEmpty())
			request.getRequestDispatcher("/Update.jsp").forward(request, response);
		else if (password.isEmpty())
			request.getRequestDispatcher("/Update.jsp").forward(request, response);
		else if (password != c_password)
			request.getRequestDispatcher("/Update.jsp").forward(request, response);
		else
		{
			System.out.println(name + " " + password);
			User tempUser = (User) request.getSession().getAttribute("curUser");
			User user = userManager.getLoggedInUser(tempUser.getEmail());
			user.setName(name);
			user.setPassword(password);
			
			userManager.updateUser(user);
		}
	}

}
