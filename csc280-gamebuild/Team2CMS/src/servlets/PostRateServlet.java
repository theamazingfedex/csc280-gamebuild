package servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.Post;
import ejb.PostEJB;

@WebServlet("/PostRateServlet.do")
@DeclareRoles({"admin", "user"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
rolesAllowed = {"admin", "user"}),
@HttpMethodConstraint(value = "POST",
rolesAllowed = {"admin", "user"})})
public class PostRateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB PostEJB postEJB;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("PostRateServlet!");
		
		int id = Integer.parseInt(request.getParameter("postId"));
		int rating = Integer.parseInt(request.getParameter("rating"));
		
		System.out.println(rating);
		
		Post post = postEJB.findPost(id);
		post.submitNewRating(rating);
		postEJB.updatePost(post);
		
//		response.sendRedirect("Index");
		request.getSession().setAttribute("postid", Integer.toString(id));
		request.getRequestDispatcher("PostViewServlet").forward(request, response);
	}

}
