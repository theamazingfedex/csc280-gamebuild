package servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ejb.AboutEJB;
import entities.About;

@WebServlet("/AboutEdit")
@DeclareRoles({"admin"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
																rolesAllowed = {"admin"}),
										  @HttpMethodConstraint(value = "POST",
																rolesAllowed = {"admin"})})
public class AboutEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB AboutEJB aboutEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		About about = aboutEJB.getAbout();
		
		System.out.println(about);
		
		if(about == null) {
			about = new About();
			about.setContent("Empty...");
			aboutEJB.createAbout(about);
		}
		
		request.setAttribute("aboutContent", about.getContent());
		request.getRequestDispatcher("WEB-INF/EditAbout.jsp").forward(request, response);
	}

}
