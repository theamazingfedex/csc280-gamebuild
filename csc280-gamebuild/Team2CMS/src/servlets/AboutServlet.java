package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.AboutEJB;
import ejb.SettingsEJB;
import entities.About;
import entities.About;

@WebServlet("/About")
public class AboutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB AboutEJB aboutEJB;
	@EJB SettingsEJB settingsEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		String enabled = settingsEJB.getSettings().getAboutEnabled();
		
		if(!enabled.equals("Disabled")) {
			About about = aboutEJB.getAbout();
			
			if(about == null) {
				about = new About();
				about.setContent("Empty...");
				aboutEJB.createAbout(about);
			}
			
			request.setAttribute("aboutContent", about.getContent());
			request.getRequestDispatcher("WEB-INF/About.jsp").forward(request, response);
		}
		
	}

}
