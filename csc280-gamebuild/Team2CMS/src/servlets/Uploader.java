package servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Scanner;

import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/Upload.do")
@MultipartConfig()
@DeclareRoles({"admin"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
rolesAllowed = {"admin"}),
@HttpMethodConstraint(value = "POST",
rolesAllowed = {"admin"})})
public class Uploader extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Collection<Part> parts = request.getParts();	
		Part uploaded = request.getPart("fname");
		
		String type = uploaded.getContentType();
		if (type==null || !type.startsWith("image")) {
			response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
			return;
		}
		
		String name = handleBrowserFileNaming(uploaded);
		File fullName = new File(name);
		name = fullName.getName();
		
		String path = "/images";
		String realPath = getServletContext().getRealPath(path);
		System.out.println(realPath);
		//String userName = (String) request.getSession().getAttribute("name");
		//String userPath = realPath + "/" + userName;
		
		File finalLocation = new File(realPath, name);
		
		System.out.println(finalLocation.toString());
		
		if(finalLocation.exists()) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			System.out.println("Already exists!");
			return;
		}
		
		OutputStream saveToDisk = new FileOutputStream(finalLocation);
		
		InputStream clientFile = uploaded.getInputStream();
		
		byte[] buffer = new byte[2048];
		int read;
		do{
			read = clientFile.read(buffer);
			if(read > 0) saveToDisk.write(buffer, 0, read);
		} while(read >= 0);
		
		clientFile.close();
		saveToDisk.flush();
		saveToDisk.close();
		
		response.sendRedirect("Gallery.do");
	}

	static String getPartText(Part p) throws IOException {
		InputStream input = p.getInputStream();
		Scanner scan = new Scanner(input);
		scan.useDelimiter("$");		
		return scan.next();
	}
	
	static String handleBrowserFileNaming(Part p) {
		String header = p.getHeader("content-disposition");
		for(String sub : header.split(";")) {
			if(sub.trim().startsWith("filename")) {
				return sub.substring(sub.indexOf('=')+1).trim().replace("\"", "");
			}
		}
		return "";
	}
}
