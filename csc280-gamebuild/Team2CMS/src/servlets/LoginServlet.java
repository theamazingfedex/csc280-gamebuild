package servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import ejb.UserEJB;
import entities.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
@DeclareRoles({"admin", "user"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
																rolesAllowed = {"admin", "user"}),
										  @HttpMethodConstraint(value = "POST",
																rolesAllowed = {"admin", "user"})})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UserEJB userManager;
   
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In login servlet");
		User curUser = (User) request.getSession().getAttribute("curUser");
		if(curUser != null)
			response.sendRedirect(request.getServletContext().getContextPath());
		else
		{
			curUser = new User();
			
//			System.out.println("email: "+email + "\npassword: "+Encryption.digest(password));
//			request.login(email, password);
			curUser = userManager.getLoggedInUser(request.getRemoteUser());
			request.getSession().setAttribute("curUser", curUser);
				
			
		}
		request.getRequestDispatcher("/Index").forward(request, response);
	}

}
