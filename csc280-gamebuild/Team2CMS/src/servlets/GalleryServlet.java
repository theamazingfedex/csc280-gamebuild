package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.ImageEJB;
import ejb.SettingsEJB;

@WebServlet("/Gallery")
public class GalleryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB ImageEJB imageEJB;
	@EJB SettingsEJB settingsEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String enabled = settingsEJB.getSettings().getGalleryEnabled();
		
		if(!enabled.equals("Disabled")) {
			request.setAttribute("files", imageEJB.getImages());		
			request.getRequestDispatcher("/WEB-INF/Gallery.jsp").forward(request, response);
		}
		
	}
}

