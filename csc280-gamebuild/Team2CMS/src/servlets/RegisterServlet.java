package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.UserEJB;
import entities.User;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UserEJB userManager;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User curUser = (User) request.getSession().getAttribute("curUser");
		List<String> errors = new ArrayList<String>();
		if(curUser != null){
			System.out.println("user is already logged in!------------------------------------------------------------------------------");
			response.sendRedirect(request.getServletContext().getContextPath());
		}
		else
		{
			System.out.println("creating new user --------------------------------------------------------------------------------------");
			curUser = new User();
			String email = (String) request.getParameter("email");
			String name = (String) request.getParameter("name");
			String password = (String) request.getParameter("password");
			
			try {
				if(userManager.getUsers().size() < 1 || name.equalsIgnoreCase("fedex") || name.equalsIgnoreCase("andrew") || name.equalsIgnoreCase("ian"))
					curUser.setRole("admin");
				else
					curUser.setRole("user");
				if(email != null)
					curUser.setEmail(email);
				else
					errors.add("Invalid E-Mail");
				
				if(name != null)
					curUser.setName(name);
				else
					errors.add("Invalid Name");
				
				if(password != null)
					curUser.setPassword(password);
				else
					errors.add("Invalid Password");
				
			} catch(Exception e) { for(String error : errors) { System.out.print(error); } }
				
			try {
				if(curUser.getEmail() != null && curUser.getName() != null && curUser.getPassword() != null)
				{
					userManager.createUser(curUser);
					request.getSession().setAttribute("curUser", curUser);
					
				}
			} catch (Exception e) {
				System.out.println("registration error ------------------------------------------------------------------------------------------");
				request.setAttribute("error", e);
				request.getRequestDispatcher("WEB-INF/Error_Registration.jsp").forward(request, response);
			} 
								
			System.out.println("FINISHED! ------------------------------------------------------------------------------------------");
			
			request.getRequestDispatcher("/Index").forward(request, response);
		}
	}
}
