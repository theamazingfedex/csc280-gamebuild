package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.ImageEJB;
import ejb.SettingsEJB;

@WebServlet("/UploadImage.jsp")
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
rolesAllowed = {"admin"}),
@HttpMethodConstraint(value = "POST",
rolesAllowed = {"admin"})})
public class GalleryUploadImagesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB ImageEJB imageEJB;
	@EJB SettingsEJB settingsEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String enabled = settingsEJB.getSettings().getGalleryEnabled();
		
		if(!enabled.equals("Disabled")) {
			request.setAttribute("files", imageEJB.getImages());		
			request.getRequestDispatcher("/WEB-INF/UploadImage.jsp").forward(request, response);
		}
		
	}
}

