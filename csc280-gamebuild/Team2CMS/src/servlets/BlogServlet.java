package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.PostEJB;
import ejb.SettingsEJB;

@WebServlet({"/Blog", "/Index"})
public class BlogServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@EJB PostEJB postEJB;
	@EJB SettingsEJB settingsEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String enabled = settingsEJB.getSettings().getBlogEnabled();
		
		if(!enabled.equals("Disabled")) {
			request.setAttribute("posts", postEJB.getPosts());
			request.getRequestDispatcher("/WEB-INF/Blog.jsp").forward(request, response);
		}	
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doGet(req, resp);
	}
}
