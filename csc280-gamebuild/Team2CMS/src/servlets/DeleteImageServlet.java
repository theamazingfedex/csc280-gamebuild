package servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.ImageEJB;
import entities.Post;
import entities.Image;

@WebServlet("/DeleteImage.do/*")
@DeclareRoles({"admin"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
rolesAllowed = {"admin"}),
@HttpMethodConstraint(value = "POST",
rolesAllowed = {"admin"})})
public class DeleteImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB private ImageEJB imageEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.print(request.getPathInfo().split("/")[1]);
		int id = Integer.parseInt(request.getPathInfo().split("/")[1]);
		Image image = imageEJB.findImage(id);	
		if(image != null){
			imageEJB.deleteImage(image);
		}
		
		response.sendRedirect("../Gallery");
	}

}
