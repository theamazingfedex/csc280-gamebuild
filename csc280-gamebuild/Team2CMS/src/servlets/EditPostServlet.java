package servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ejb.PostEJB;
import entities.Post;

@WebServlet("/EditPostServlet.do")
@DeclareRoles({"admin"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
rolesAllowed = {"admin"}),
@HttpMethodConstraint(value = "POST",
rolesAllowed = {"admin"})})
public class EditPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB PostEJB postEJB;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("postid"));
		Post post = postEJB.findPost(id);		
		request.setAttribute("thetitle", post.getTitle());
		request.setAttribute("theid", post.getPostId());
		request.setAttribute("thecontent", post.getPostContent());
		request.getRequestDispatcher("EditPost.jsp").forward(request, response);
	}

}
