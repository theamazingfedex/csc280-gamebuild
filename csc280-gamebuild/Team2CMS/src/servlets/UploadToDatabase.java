package servlets;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import entities.Image;
import ejb.ImageEJB;

@WebServlet("/UploadToDatabase.do")
@DeclareRoles({"admin"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "POST",
																rolesAllowed = {"admin"}),
										  @HttpMethodConstraint(value = "GET",
																rolesAllowed = {"admin"})})
public class UploadToDatabase extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB ImageEJB imageManger;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get File Location
		Collection<Part> parts = request.getParts();	
		Part uploaded = request.getPart("fname");
		File fileLocation = getFullFilePath(uploaded);
		System.out.println(fileLocation.toString());
		
		// Get Description
		String description = request.getParameter("description");
		System.out.println(description);
		
		String contentType = uploaded.getContentType();
		
		byte[] content = getBytes(uploaded.getInputStream());
		
		Image img = new Image();
		img.setDescription(description);
		img.setContentType(contentType);
		img.setContent(content);
		
		imageManger.createImage(img);
		
		response.sendRedirect("Gallery");
	}
	
	byte[] getBytes(InputStream instream) {		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		try {
			while ((nRead = instream.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			buffer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return buffer.toByteArray();
	}
	
	File getFullFilePath(Part uploaded) {
				
		String type = uploaded.getContentType();
		if (type==null || !type.startsWith("image")) { return null; }
		
		String name = handleBrowserFileNaming(uploaded);
		File fullName = new File(name);
		name = fullName.getName();
		
		String path = "/images";
		String realPath = getServletContext().getRealPath(path);
		
		return new File(realPath, name);
	}
	
	static String handleBrowserFileNaming(Part p) {
		String header = p.getHeader("content-disposition");
		for(String sub : header.split(";")) {
			if(sub.trim().startsWith("filename")) {
				return sub.substring(sub.indexOf('=')+1).trim().replace("\"", "");
			}
		}
		return "";
	}

}
