package servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.SettingsEJB;
import entities.Settings;

@WebServlet("/SettingsSaveServlet.do")
@DeclareRoles({"admin"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "GET",
rolesAllowed = {"admin"}),
@HttpMethodConstraint(value = "POST",
rolesAllowed = {"admin"})})
public class SettingsSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB private SettingsEJB settingsEJB;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("Title");
		String colorTheme = request.getParameter("ColorTheme");
		
		Settings settings = settingsEJB.getSettings();

		if(title != null && !(title.isEmpty())){
			settings.setTitle(title);
		}
		
		settings.setColorTheme(colorTheme);
		
		settings.setBlogEnabled(request.getParameter("BlogEnabled"));
		settings.setGalleryEnabled(request.getParameter("GalleryEnabled"));
		settings.setAboutEnabled(request.getParameter("AboutEnabled"));
		settings.setCommentsEnabled(request.getParameter("CommentsEnabled"));
		
		settingsEJB.updateSettings(settings);
		
		response.sendRedirect("Settings");
	}

}
