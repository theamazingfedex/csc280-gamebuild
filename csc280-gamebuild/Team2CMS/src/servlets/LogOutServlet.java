package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.UserEJB;
import entities.User;

/**
 * Servlet implementation class LogOutServlet
 */
@WebServlet("/LogOut")
public class LogOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@EJB
	UserEJB userManager;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User curUser = (User) request.getSession().getAttribute("curUser");
		request.getSession().setAttribute("curUser", null);
		if(curUser != null)
		{
			request.logout();
			request.getSession().invalidate();
		}
		request.getRequestDispatcher("/Index").forward(request, response);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}
}
