package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

import entities.Post;

@Stateless
public class PostEJB {
	@PersistenceContext
	private EntityManager em;
		
	public void createPost(Post post){
		if(post != null){
			em.persist(post);
			em.flush();
		}
	}
	
	public Post updatePost(Post post){
		Post p = em.merge(post);
		em.flush();
		return (p);
	}
	
	public void deletePost(Post post){
		em.remove(em.merge(post));
		em.flush();
	}
	
	public Post findPost(int id){
		Post post = em.find(Post.class, id);
		return post;
	}

	public List<Post> getPosts() {
		TypedQuery<Post> getPosts = em.createQuery("SELECT p FROM Post p", Post.class);
		return getPosts.getResultList();
	}
	
}
