package ejb;

import javax.ejb.Stateless;
import javax.persistence.*;

import entities.Settings;

@Stateless
public class SettingsEJB {
	@PersistenceContext
	private EntityManager em;
	
	public Settings createSettings(Settings settings){
		em.persist(settings);
		em.flush();
		return settings;
	}
	
	public Settings updateSettings(Settings settings){
		em.merge(settings);
		em.flush();
		return settings;
	}
	
	public void deleteSettings(Settings settings){
		em.remove(em.merge(settings));
		em.flush();
	}
	
	public Settings getSettings(){
		TypedQuery<Settings> getSettings = em.createQuery("SELECT x FROM Settings x", Settings.class);
		if(getSettings.getResultList().size() > 0) {
			return getSettings.getResultList().get(0);
		}
		return null;
		
	}
}
