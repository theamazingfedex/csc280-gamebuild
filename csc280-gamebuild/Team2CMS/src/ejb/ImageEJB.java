package ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.*;

import entities.Image;
import entities.Post;

@Stateless
public class ImageEJB {
	@PersistenceContext
	private EntityManager em;
	
	public Image createImage(Image image){
		em.persist(image);
		em.flush();
		return image;
	}
	
	public Image updateImage(Image image){
		em.merge(image);
		em.flush();
		return image;
	}
	
	public void deleteImage(Image image){
		em.remove(em.merge(image));
		em.flush();
	}
	
	public Image findImage(int id){
		Image image = em.find(Image.class, id);
		return image;
	}
	
	public List<Image> getImages(){
		TypedQuery<Image> getImages = em.createQuery("SELECT x FROM Image x", Image.class);
		return getImages.getResultList();
	}
	
	public int getSize() {
		return -1;
	}
}
