package ejb;

import javax.ejb.Stateless;
import javax.persistence.*;

import entities.About;

@Stateless
public class AboutEJB {
	@PersistenceContext
	private EntityManager em;
	
	public About createAbout(About about){
		em.persist(about);
		em.flush();
		return about;
	}
	
	public About updateAbout(About about){
		em.merge(about);
		em.flush();
		return about;
	}
	
	public void deleteAbout(About about){
		em.remove(em.merge(about));
		em.flush();
	}
	
	public About getAbout(){
		TypedQuery<About> getAbout = em.createQuery("SELECT x FROM About x", About.class);
		if(getAbout.getResultList().size() > 0) {
			return getAbout.getResultList().get(0);
		}
		return null;
		
	}
}
