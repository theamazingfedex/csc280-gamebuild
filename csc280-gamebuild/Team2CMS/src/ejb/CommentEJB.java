package ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.Comment;
import entities.User;

@Stateless
public class CommentEJB {
	@PersistenceContext
	private EntityManager em;
	
	public Comment createComment(Comment comment){
		em.persist(comment);
		em.flush();
		return comment;
	}
	
	public Comment updateComment(Comment comment){
		Comment c = em.merge(comment);
		em.flush();
		return (c);
	}
	
	public void deleteComment(Comment comment){
		comment = em.getReference(Comment.class, comment.getCommentId());
		comment.getParentalPost().getComments().remove(comment);
		em.remove(comment);
		em.flush();
	}
	
	public void deleteComment(int commentId){
		Comment comment = em.getReference(Comment.class, commentId);
		comment.getParentalPost().getComments().remove(comment);
		em.remove(comment);
		em.flush();
	}
	
	public Comment findComment(int id){
		Comment comment = em.find(Comment.class, id);
		return comment;
	}
	
	public List<Comment> getPostComments(int postId)
	{
		TypedQuery<Comment> getComment = em.createQuery("SELECT c FROM Comment c WHERE c.parentalPost.postid == postId", Comment.class);
		return (List<Comment>) getComment.getResultList().get(0);
	}
}
