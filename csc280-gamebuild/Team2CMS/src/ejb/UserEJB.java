package ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import security.Encryption;

import entities.User;

@Stateless
public class UserEJB {
	@PersistenceContext
	private EntityManager em;
	
	public User createUser(User user){
		em.persist(user);
		em.flush();
		return user;
	}
	
	public void deleteUser(int userId){
		em.remove(getUser(userId));
		em.flush();
	}
	
	public User updateUser(User user){
		User u = em.merge(user);
		em.flush();
		return u;
	}
	public User getUser(int userId)
	{
		return em.find(User.class, userId);
	}
	
	public User getAnonUser(String email, String password){
		return em.find(User.class, email);
	}

	public User getLoggedInUser(String email, String password){
		User tempUser = null;
		TypedQuery<User> getUsers = em.createQuery("SELECT u FROM User u", User.class);
		for(User u : getUsers.getResultList()){
			if (u.getEmail().equals(email) && u.getPassword().equals(Encryption.digest(password)));
				tempUser = u;
		}
		return tempUser;
	}
	public User getLoggedInUser(String email){
		User tempUser = null;
		TypedQuery<User> getUsers = em.createQuery("SELECT u FROM User u WHERE u.email= :email", User.class);
		getUsers.setParameter("email", email);
		tempUser = getUsers.getSingleResult();
		return tempUser;
	}
	
	public List<User> getUsers()
	{
		TypedQuery<User> getUsers = em.createQuery("SELECT u FROM User u", User.class);
		return getUsers.getResultList();
	}
}
