package filters;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import ejb.SettingsEJB;
import ejb.UserEJB;
import entities.Settings;

@WebFilter(filterName = "SettingsFilter", urlPatterns = { "/*" })
public class SettingsFilter implements Filter {

	@EJB UserEJB userEJB;
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("Gogo SettingsFilter!");
		
		//Settings settings = userEJB.get...
		
		//System.out.println(settings);
		

		//request.setAttribute("settings", settings);
		
		chain.doFilter(request, response);
	}

    public SettingsFilter() {}

    public void init(FilterConfig fConfig) throws ServletException {}
    
	public void destroy() {}

}
