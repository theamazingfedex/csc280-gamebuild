package filters;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import ejb.SettingsEJB;
import entities.Settings;

@WebFilter(filterName = "SecurityFilter", urlPatterns = { "/*" })
public class UserSecurityFilter implements Filter {

	@EJB SettingsEJB settingsEJB;
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("Gogo SecurityFilter!");
		
		Settings settings = settingsEJB.getSettings();
		
		System.out.println(settings);
		
		if(settings == null) {
			settings = new Settings();
			settings.setTitle("XXX");
			settings.setColorTheme("Default");
			settings.setBlogEnabled("Everyone");
			settings.setGalleryEnabled("Everyone");
			settings.setAboutEnabled("Disabled");
			settings.setCommentsEnabled("Everyone");
			settingsEJB.createSettings(settings);
		}
		
		request.setAttribute("settings", settings);
		
		chain.doFilter(request, response);
	}

    public UserSecurityFilter() {}

    public void init(FilterConfig fConfig) throws ServletException {}
    
	public void destroy() {}

}
